require 'librarian/chef/integration/knife'
root = File.absolute_path(File.dirname(__FILE__))

file_cache_path root
cookbook_path root + '/cookbooks'
cookbook_path [ Librarian::Chef.install_path, "#{root}/../cookbooks-overrides" ]

